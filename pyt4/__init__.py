# -*- coding: utf-8 -*-
from StringIO import StringIO
from itertools import groupby
from _tables import decodeTables, encodeTables

__author__ = 'viruzzz-kun'


def bitReader(fd):
    """Generator function producing stream of bit values

    @param fd: file-like object, having read method
    @return: bit stream"""
    while True:
        byteStream = fd.read(8192)
        if len(byteStream) == 0:
            raise StopIteration
        for char in byteStream:
            byte = ord(char)
            for bitN in xrange(8):
                yield (byte >> bitN) & 1


def bitWriter(fd, bitStream):
    """Writes input bitstream to file-line object

    @param fd: output file-like object, having write method
    @param bitStream: iterable of bits (zeros and ones)
    @return: None"""
    bitN = 0
    buf = []
    for bit in bitStream:
        buf.append(bit)
        bitN += 1
        if bitN == 8:
            byte = reduce(lambda word, bit: word << 1 | bit, buf, 0)
            fd.write(chr(byte))
            bitN = 0
            buf = []


class Decoder:
    """Decoder of RFC804-encoded data"""

    def __init__(self, lineWidth):
        """Constructor

        @param lineWidth: width of data line in pixels"""
        self.lineWidth = lineWidth

    def decode(self, bitStream):
        out = StringIO()
        bitWriter(out, self.decodeGen(bitStream))
        return out.getvalue()

    def decodeGen(self, bitStream):
        """Generator. Decode T4 data

        @param bitStream: iterable of encoded bits
        @return: iterable of decoded bits"""
        for (bit, rl) in self.__decodeInt(bitStream):
            for i in xrange(rl):
                yield bit

    def __decodeInt(self, bitStream):
        wordLength = 0
        word = ''
        # 1 = white, 0 = black
        color = 1
        lineNo = 0
        lineRun = 0
        bitCount = 0
        for bit in bitStream:
            bitCount += 1
            char = chr(bit + ord('0'))
            word += char
            wordLength += 1
            if wordLength == 12 and word == '000000000001':
                word = ''
                wordLength = 0
                lineNo += 1
                lineRun = 0
                color = 1
                yield (color, self.lineWidth - lineRun)
                continue
            if color == 1:
                if wordLength in decodeTables[0] and word in decodeTables[0][wordLength]:
                    run = min(decodeTables[0][wordLength][word], self.lineWidth - lineRun)
                    yield (color, run)
                    word = ''
                    wordLength = 0
                    color = 0
                    lineRun += run
                    continue
                elif wordLength in decodeTables[1] and word in decodeTables[1][wordLength]:
                    run = min(decodeTables[1][wordLength][word], self.lineWidth - lineRun)
                    yield (color, run)
                    word = ''
                    wordLength = 0
                    lineRun += run
                    continue
            elif color == 0:
                if wordLength in decodeTables[2] and word in decodeTables[2][wordLength]:
                    run = min(decodeTables[2][wordLength][word], self.lineWidth - lineRun)
                    yield (color, run)
                    word = ''
                    wordLength = 0
                    color = 1
                    lineRun += run
                    continue
                elif wordLength in decodeTables[3] and word in decodeTables[3][wordLength]:
                    run = min(decodeTables[3][wordLength][word], self.lineWidth - lineRun)
                    yield (color, run)
                    word = ''
                    wordLength = 0
                    lineRun += run
                    continue
            if wordLength in decodeTables[4] and word in decodeTables[4][wordLength]:
                run = min(decodeTables[4][wordLength][word], self.lineWidth - lineRun)
                yield (color, run)
                word = ''
                wordLength = 0
                lineRun += run
                continue
            if wordLength > 13 and '000000000001' in word:
                i = word.index('000000000001') + 12
                word = word[i:]
                wordLength = len(word)
                lineRun = 0
                lineNo += 1
                color = 1


class Encoder:
    lengths = reversed(sorted(encodeTables[0].values()))

    def encode(self, imgData, width):
        out = StringIO()
        bitWriter(out, self.bitProduce(self.encodeInt(imgData, width)))
        return out.getvalue()

    def bitProduce(self, pair):
        for bit, rl in pair:
            while rl > 0:
                for l in self.lengths:
                    if l <= rl:
                        for c in encodeTables[bit][l]:
                            yield ord(c) - ord('0')
                        rl -= l
                        break

    def encodeInt(self, imgData, width):
        x_pos = 0
        rl = 0
        cc = 1
        for bit in bitReader(StringIO(imgData)):
            x_pos += 1
            if cc == bit:
                rl += 1
            if x_pos == width:
                yield (bit, rl)
                x_pos = 0
                cc = 1
                rl = 0
                yield None

def main():
    decoder = Decoder(2400)
    with open('file.t4', 'rb') as file_in:
        with open('file.raw', 'wb') as file_out:
            bitWriter(file_out, decoder.decodeGen(bitReader(file_in)))


if __name__ == "__main__":
    main()
